import Table from "./components/Table.jsx";
import AddForm from "./components/AddForm.jsx";

function App() {
  return (
    <div className="App">
      <h1 className="text-danger text-center mt-2">Reading List</h1>
      <hr />
      <AddForm />
      <Table />
    </div>
  );
}

export default App;
