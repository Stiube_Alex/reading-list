import React from "react";
import { db } from "../firebase.js";
import { collection, query, where, getDoc, getDocs, doc, deleteDoc, setDoc } from "firebase/firestore";

export default function Table() {
  let [books, setBooks] = React.useState();
  let [year, setYear] = React.useState();

  const nameData = React.useRef();
  const authorData = React.useRef();

  let clickedYearData = [];
  const changeBooksByYear = async (event) => {
    const q = query(collection(db, "books"), where("year", "==", ~~event.target.dataset["year"]));
    const querySnapshot = await getDocs(q);
    querySnapshot.forEach((doc) => {
      clickedYearData.push(doc);
    });

    setBooks(
      clickedYearData.map((el, index) => (
        <div key={index} id={el.id} className="row cover-text-fix">
          <div className="col-sm">
            <img className="coverImg" src={el.data().image} alt="" />{" "}
          </div>
          <div className="col-sm">{el.data().name}</div>
          <div className="col-sm">{el.data().author}</div>
          <div className="col-sm">{el.data().data}</div>
          <div className="col-sm">
            <svg onClick={editBook} className="action-icons" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">
              <path d="M362.7 19.32C387.7-5.678 428.3-5.678 453.3 19.32L492.7 58.75C517.7 83.74 517.7 124.3 492.7 149.3L444.3 197.7L314.3 67.72L362.7 19.32zM421.7 220.3L188.5 453.4C178.1 463.8 165.2 471.5 151.1 475.6L30.77 511C22.35 513.5 13.24 511.2 7.03 504.1C.8198 498.8-1.502 489.7 .976 481.2L36.37 360.9C40.53 346.8 48.16 333.9 58.57 323.5L291.7 90.34L421.7 220.3z" />
            </svg>
            <svg onClick={deleteBook} className="action-icons" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512">
              <path d="M135.2 17.69C140.6 6.848 151.7 0 163.8 0H284.2C296.3 0 307.4 6.848 312.8 17.69L320 32H416C433.7 32 448 46.33 448 64C448 81.67 433.7 96 416 96H32C14.33 96 0 81.67 0 64C0 46.33 14.33 32 32 32H128L135.2 17.69zM31.1 128H416V448C416 483.3 387.3 512 352 512H95.1C60.65 512 31.1 483.3 31.1 448V128zM111.1 208V432C111.1 440.8 119.2 448 127.1 448C136.8 448 143.1 440.8 143.1 432V208C143.1 199.2 136.8 192 127.1 192C119.2 192 111.1 199.2 111.1 208zM207.1 208V432C207.1 440.8 215.2 448 223.1 448C232.8 448 240 440.8 240 432V208C240 199.2 232.8 192 223.1 192C215.2 192 207.1 199.2 207.1 208zM304 208V432C304 440.8 311.2 448 320 448C328.8 448 336 440.8 336 432V208C336 199.2 328.8 192 320 192C311.2 192 304 199.2 304 208z" />
            </svg>
          </div>
        </div>
      ))
    );

    clickedYearData = [];
  };

  const loadYears = async () => {
    const yearsData = [];
    const years = [];

    const querySnapshot = await getDocs(query(collection(db, "books")));
    querySnapshot.forEach((doc) => {
      yearsData.push(doc.data());
    });

    yearsData.sort();
    setYear(
      yearsData.map((el, index) => {
        if (!years.includes(el.year)) {
          years.push(el.year);
          if (el.year) {
            return (
              <div key={index}>
                <button data-year={el.year} className="btn btn-outline-danger my-1" onClick={changeBooksByYear}>
                  {el.year}
                </button>
              </div>
            );
          }
        }
        return null;
      })
    );
  };

  const editBook = (event) => {
    localStorage.setItem("currentId", event.currentTarget.parentNode.parentNode.id);

    document.getElementById("editForm").style.display = "block";
    document.getElementById("addForm").style.display = "none";

    nameData.current.value = event.currentTarget.parentNode.parentNode.children[1].textContent;
    authorData.current.value = event.currentTarget.parentNode.parentNode.children[2].textContent;
  };

  const deleteBook = (event) => {
    localStorage.setItem("currentId", event.currentTarget.parentNode.parentNode.id);
    document.getElementById("deleteBook").style.display = "block";
  };

  const currentYearData = [];
  const loadBooksFromCurrentYear = async () => {
    const currentYear = new Date().getFullYear();
    const querySnapshot = await getDocs(query(collection(db, "books"), where("year", "==", currentYear)));
    querySnapshot.forEach((doc) => {
      currentYearData.push(doc);
    });

    setBooks(
      currentYearData.map((el, index) => (
        <div key={index} id={el.id} className="row cover-text-fix">
          <div className="col-sm">
            <img className="coverImg" src={el.data().image} alt="" />{" "}
          </div>
          <div className="col-sm">{el.data().name}</div>
          <div className="col-sm">{el.data().author}</div>
          <div className="col-sm">{el.data().data}</div>
          <div className="col-sm">
            <svg onClick={editBook} className="action-icons" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">
              <path d="M362.7 19.32C387.7-5.678 428.3-5.678 453.3 19.32L492.7 58.75C517.7 83.74 517.7 124.3 492.7 149.3L444.3 197.7L314.3 67.72L362.7 19.32zM421.7 220.3L188.5 453.4C178.1 463.8 165.2 471.5 151.1 475.6L30.77 511C22.35 513.5 13.24 511.2 7.03 504.1C.8198 498.8-1.502 489.7 .976 481.2L36.37 360.9C40.53 346.8 48.16 333.9 58.57 323.5L291.7 90.34L421.7 220.3z" />
            </svg>
            <svg onClick={deleteBook} className="action-icons" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512">
              <path d="M135.2 17.69C140.6 6.848 151.7 0 163.8 0H284.2C296.3 0 307.4 6.848 312.8 17.69L320 32H416C433.7 32 448 46.33 448 64C448 81.67 433.7 96 416 96H32C14.33 96 0 81.67 0 64C0 46.33 14.33 32 32 32H128L135.2 17.69zM31.1 128H416V448C416 483.3 387.3 512 352 512H95.1C60.65 512 31.1 483.3 31.1 448V128zM111.1 208V432C111.1 440.8 119.2 448 127.1 448C136.8 448 143.1 440.8 143.1 432V208C143.1 199.2 136.8 192 127.1 192C119.2 192 111.1 199.2 111.1 208zM207.1 208V432C207.1 440.8 215.2 448 223.1 448C232.8 448 240 440.8 240 432V208C240 199.2 232.8 192 223.1 192C215.2 192 207.1 199.2 207.1 208zM304 208V432C304 440.8 311.2 448 320 448C328.8 448 336 440.8 336 432V208C336 199.2 328.8 192 320 192C311.2 192 304 199.2 304 208z" />
            </svg>
          </div>
        </div>
      ))
    );
  };

  window.addEventListener("load", () => {
    loadYears();
    loadBooksFromCurrentYear();
  });

  const cancelDeleteBook = async () => {
    document.getElementById("deleteBook").style.display = "none";
  };

  const confirmlDeleteBook = async () => {
    const id = localStorage.getItem("currentId");
    await deleteDoc(doc(db, "books", id));
    localStorage.clear();
    document.getElementById("deleteBook").style.display = "none";
    window.location.reload();
  };

  const deleteForm = (
    <div id="deleteBook">
      <div className="delete-popin">
        <p>Are you sure you want to delete this?</p>
        <div onClick={confirmlDeleteBook} className="btn btn-outline-danger">
          Delete
        </div>
        <div onClick={cancelDeleteBook} className="btn btn-outline-success">
          Cancel
        </div>
      </div>
      <div className="back"></div>
    </div>
  );

  const edit = async () => {
    const item = await doc(db, "books", localStorage.getItem("currentId"));
    const originalDoc = await getDoc(item);

    await setDoc(item, {
      name: nameData.current.value + " *",
      author: authorData.current.value,
      data: originalDoc.data().data,
      year: originalDoc.data().year,
      image: originalDoc.data().image,
    });

    localStorage.clear();
    document.getElementById("editForm").style.display = "none";
    document.getElementById("addForm").style.display = "block";

    window.location.reload();
  };

  const cancelEdit = () => {
    localStorage.clear();
    document.getElementById("editForm").style.display = "none";
    document.getElementById("addForm").style.display = "block";
  };

  const editForm = (
    <>
      <div id="editForm">
        <div className="input-group m-4 w-50 mx-auto">
          <input ref={nameData} type="text" className="form-control" placeholder="Name" />
          <input ref={authorData} type="text" className="form-control" placeholder="Author" />
          <div className="input-group-append">
            <button className="btn custom-file-upload cancel-btn-border-fix" type="button" onClick={cancelEdit}>
              Cancel
            </button>
            <button className="btn btn-outline-danger corners-fix" type="button" onClick={edit}>
              Change
            </button>
          </div>
        </div>
      </div>
    </>
  );

  return (
    <>
      {editForm}
      {deleteForm}
      <div className="grid">
        <div className="container">{year}</div>
        <div className="container text-center my-3">
          <div className="row">
            <div className="col-sm header-text">Cover</div>
            <div className="col-sm header-text">Name</div>
            <div className="col-sm header-text">Author</div>
            <div className="col-sm header-text">Data</div>
            <div className="col-sm header-text">Action</div>
          </div>
          {books}
        </div>
      </div>
    </>
  );
}
