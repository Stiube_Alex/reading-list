import React from "react";
import { db } from "../firebase.js";
import { collection, addDoc } from "firebase/firestore";

export default function AddForm() {
  const nameData = React.useRef();
  const authorData = React.useRef();
  const imgInput = React.useRef();

  const add = async () => {
    if (nameData.current.value && authorData.current.value) {
      const data = new Date();
      const monthNames = [
        "January",
        "February",
        "March",
        "April",
        "May",
        "June",
        "July",
        "August",
        "September",
        "October",
        "November",
        "December",
      ];

      const formatedData = data.getDate() + "  " + monthNames[data.getMonth()];
      const currentYear = data.getFullYear();
      const jsonData = require("../emptyPhoto.json");

      if (imgInput.current.files[0]) {
        const reader = new FileReader();
        reader.readAsDataURL(imgInput.current.files[0]);
        reader.onloadend = () => {
          addDoc(collection(db, "books"), {
            name: nameData.current.value,
            author: authorData.current.value,
            data: formatedData,
            year: currentYear,
            image: reader.result,
          }).then(() => {
            window.location.reload();
          });
        };
      } else {
        await addDoc(collection(db, "books"), {
          name: nameData.current.value,
          author: authorData.current.value,
          data: formatedData,
          year: currentYear,
          image: jsonData.code,
        });
        window.location.reload();
      }
    }
  };

  return (
    <>
      <div id="addForm">
        <div className="input-group m-4 w-50 mx-auto">
          <input ref={nameData} type="text" className="form-control" placeholder="Name" />
          <input ref={authorData} type="text" className="form-control" placeholder="Author" />
          <label class="custom-file-upload">
            <input ref={imgInput} type="file" id="img" name="img" accept="image/*"></input>
            Image
          </label>
          <div className="input-group-append">
            <button className="btn btn-outline-danger corners-fix" type="button" onClick={add}>
              Add
            </button>
          </div>
        </div>
      </div>
    </>
  );
}
