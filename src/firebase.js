import { initializeApp } from "firebase/app";
import { getFirestore } from "firebase/firestore";

const firebaseConfig = {
  apiKey: "AIzaSyDFE3NoUzzrK0zCt3x3kiECGcojbV-eVtE",
  authDomain: "reading-list-92d01.firebaseapp.com",
  projectId: "reading-list-92d01",
  storageBucket: "reading-list-92d01.appspot.com",
  messagingSenderId: "1047386533331",
  appId: "1:1047386533331:web:3046c434b56a4c161ed326",
};

const app = initializeApp(firebaseConfig);

export const db = getFirestore(app);
